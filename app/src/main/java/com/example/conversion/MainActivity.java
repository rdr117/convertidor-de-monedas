package com.example.conversion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private TextView lblSelecciona;
    private EditText txtCantidad;
    private Button btnPulsame;
    private Button btnLimpiar;
    private Button btnCerrar;
    private Spinner spnMoneda;
    private TextView lblTotal;
    private long opcion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Spinner Trabajando
        spnMoneda =(Spinner) findViewById(R.id.spnMoneda);
        ArrayAdapter<String> Adaptador=new
                ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_expandable_list_item_1
                ,getResources().getStringArray(R.array.moneda));
        spnMoneda.setAdapter(Adaptador);
        spnMoneda.setOnItemSelectedListener(new
                                                    AdapterView.OnItemSelectedListener() {
                                                        @Override
                                                        public void onItemSelected(AdapterView<?> adapterView, View view,
                                                                                   int i, long l) {
                                                            Toast.makeText(MainActivity.this, "Selecciono la moneda " +
                                                                            adapterView.getItemAtPosition(i).toString(),
                                                                    Toast.LENGTH_SHORT).show();
                                                        }

                                                        @Override
                                                        public void onNothingSelected(AdapterView<?> adapterView) {
                                                        }

                                                    });
        //Botones Ligados
        lblSelecciona=(TextView)findViewById(R.id.lblSelecciona) ;
        txtCantidad= (EditText)findViewById(R.id.txtCantidad);
        btnPulsame= (Button)findViewById(R.id.btnPulsame);
        btnLimpiar= (Button)findViewById(R.id.btnLimpiar);
        btnCerrar= (Button)findViewById(R.id.btnCerrar);
        lblTotal= (TextView)findViewById(R.id.lblTotal);


        btnPulsame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validar
                if(txtCantidad.getText().toString().matches("")
                ){

                    Toast.makeText(MainActivity.this,"Falto capturar numero",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    float resultado=0.0f;
                            float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    switch ((int) opcion){
                        case 0: resultado = cantidad * 0.05f; break;
                        case 1: resultado = cantidad * 0.48f; break;
                        case 2: resultado = cantidad * 0.7f; break;
                        case 3: resultado = cantidad * 0.41f; break;

                    }
                    lblTotal.setText(String.valueOf(resultado));
                }
            }

        });

        //funcion para el spinner que define la posicion y lo manda al evento click de calcular
        spnMoneda.setAdapter(Adaptador);
        spnMoneda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                opcion=adapterView.getItemIdAtPosition(i);
                lblTotal.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Funcion que limpia la cantidad de pesos mxn, el total y reinicia el spinner
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                lblTotal.setText("");
                spnMoneda.setSelection(0);


            }
        });
        //Funcion para que cierre la app
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
